#!/usr/bin/env python3

import sys

from xivo.agi import AGI
from xivo.BackSQL.backpostgresql import anysql

DB_URI = 'postgresql://asterisk:proformatique@localhost:5432/asterisk'


def with_agi_params(fonction):
    connexion = db_connect(DB_URI)
    cursor = connexion.cursor()
    fonction(AGI(), cursor, sys.argv[1:])
    cursor.close()
    connexion.close()


def db_connect(db_uri):
    return anysql.connect_by_uri(db_uri)


def find_user_by_username(cursor, username):
    """
    Récupère un utilisateur dans la base de donnée depuis son username
    """
    query = f'''\
SELECT u.id, u.loginclient, e.exten, e.context, l.name \
FROM userfeatures u \
INNER JOIN user_line ul on u.id=ul.user_id and ul.main_user=true and ul.main_line=true \
INNER JOIN extensions e on e.id=ul.extension_id \
INNER JOIN linefeatures l on l.id=ul.line_id \
WHERE u.loginclient='{username}'\
'''
    cursor.query(query)
    result = cursor.fetchall()
    if len(result) < 1:
        return None
    return result


def find_group_by_name(cursor, name):
    """
    Récupère un groupe dans la base de donnée depuis son nom
    """
    query = f"SELECT id, name, number, context FROM groupfeatures WHERE name = '{name}'"
    cursor.query(query)
    result = cursor.fetchall()
    if len(result) < 1:
        return None
    return result


def get_number_from_name(agi, cursor, args):
    # TODO: Faire l'implémentation dans cette méthode
    name = args[0]
    agi.verbose(f"Name : {name}")
    agi.set_variable('AGI_TYPE', 'TODO')
    agi.set_variable('AGI_NUMBER', 'TODO')


if __name__ == '__main__':
    with_agi_params(get_number_from_name)
