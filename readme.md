# Pizza lab - Python

## Déploiement et tests manuels

1. Déployer le fichier `xivo_extrafeatures.conf` dans le dossier `/etc/asterisk/extensions_extra.d/` sur le xivo.
2. Recharger le dialplan `asterisk -rx "dialplan reload"`
3. Copier le fichier `get_number.py` dans le dossier `/var/lib/asterisk/agi-bin/`
4. Vérifier les permissions `chown asterisk:asterisk /var/lib/asterisk/agi-bin/get_number.py`
5. Pour tester : appeler depuis un UC Assistant `[name]@localhost` en remplaçant name par le username d'un user ou d'un groupe. Pour un groupe il faut connecter un user dans ce groupe pour pouvoir prendre l'appel.
   * Si vous avez appelé un user, vous devez entendre des dindons, puis l'utilisateur doit sonner
   * Si vous avez appelé un groupe, vous devez entendre des singes, puis un utilisateur du group doit sonner

## Méthode d'implémentation suggérée

1. Ajouter une structure de donnée (Enum, Dataclass) correspondant au modèle de l'exercice.
2. Utiliser ce modèle pour typer les retours des fonctions `find_user_by_username` et `find_group_by_name`. Il est conseillé de changer l'implémentation pour avoir un type de retour plus simple.
3. Implémenter `get_number_from_name` en utilisant les méthodes `find_user_by_username` et `find_group_by_name`
4. Vérifier que le test unitaire fonctionne `pytest` à la racine du projet
5. Écrire les tests unitaires manquants
