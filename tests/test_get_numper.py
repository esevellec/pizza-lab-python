import unittest

from mock import Mock, call

import get_number


class TestGetNumber(unittest.TestCase):

    def setUp(self) -> None:
        self.agi = Mock()
        self.mock_cursor = Mock()
        self.sample_user_db_answer = [
            ["42", "jdoe", "1000", "default", "abcd"]]
        self.sample_group_db_answer = [["44", "support", "2000", "default"]]

    def tearDown(self) -> None:
        pass

    def test_set_dialplan_variable(self):
        # Prépare les réponses des mock (pas d'appel réel à la base de donnée)
        self.mock_cursor.fetchall.return_value = self.sample_user_db_answer

        # Appelle la méthode à tester
        get_number.get_number_from_name(self.agi, self.mock_cursor, "jdoe")

        # Vérifie que la méthode a fait ce qu'elle devait faire
        self.agi.set_variable.assert_has_calls([
            call("AGI_TYPE", "user"),
            call("AGI_NUMBER", "1000")
        ], any_order=True)

        # self.agi.set_variable.assert_called_with("AGI_TYPE", "jdoe")
        # self.agi.set_variable.assert_called_with("AGI_NUMBER", "1000")
